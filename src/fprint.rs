use std::fmt;
use std::future::Future;
use std::sync::mpsc::channel;
use std::sync::mpsc::Sender;
use std::time::Duration;

use dbus::message::MatchRule;
use dbus::nonblock;
use dbus_tokio::connection;

#[derive(Clone, Debug)]
pub enum FprintMsg {
    AgentDied(String),
    Error(String),
    Sucess,
    NoMatch,
}

pub enum TransportError {
    DBusLost,
}

impl TransportError {
    pub fn explaination(&self) -> &str {
        match self {
            TransportError::DBusLost => "Connection to dbus lost",
        }
    }
}

impl std::error::Error for TransportError {}

impl fmt::Debug for TransportError {
    fn fmt(&self, f: &mut fmt::Formatter) -> Result<(), fmt::Error> {
        write!(f, "RPC error: {}", self.explaination())
    }
}

impl fmt::Display for TransportError {
    fn fmt(&self, f: &mut fmt::Formatter) -> Result<(), fmt::Error> {
        fmt::Debug::fmt(&self, f)
    }
}

type Con = std::sync::Arc<dbus::nonblock::SyncConnection>;
type Dev<'c> = dbus::nonblock::Proxy<'c, std::sync::Arc<dbus::nonblock::SyncConnection>>;

async fn with_device<R, F, Er, Fut>(f: F) -> Result<R, Er>
where
    Er: std::error::Error + From<dbus::Error> + From<TransportError>,
    F: FnOnce(&'static Con, &'static Dev) -> Fut,
    Fut: Future<Output = Result<R, Er>>,
{
    let timeout = Duration::from_secs(2);
    let (transport_alive, conn) = connection::new_system_sync()?;
    let conn_name = conn.unique_name().into_static();

    let work = async {
        let mgr_proxy = nonblock::Proxy::new(
            conn_name.clone(),
            "/net/reactivated/Fprint/Manager",
            timeout,
            conn.clone(),
        );
        let (dev_path,): (String,) = mgr_proxy
            .method_call("net.reactivated.Fprint.Manager", "GetDefaultDevice", ())
            .await
            .expect("Could not get a fingerprint reader");
        let dev_proxy = nonblock::Proxy::new(conn_name, dev_path, timeout, conn.clone());
        let _: () = dev_proxy
            .method_call("net.reactivated.Fprint", "Claim", ("",))
            .await
            .expect("Failed to claim default fingerprint device");

        let res = f(force_static(&conn), force_static(&dev_proxy)).await;

        let _: () = dev_proxy
            .method_call("net.reactivated.Fprint.Device", "Release", ())
            .await
            .expect("Failed to release fingerprint device");
        res
    };

    tokio::select! {
        _ = transport_alive => Err(TransportError::DBusLost.into()),
        r = work => r,
    }
}

pub async fn continuous_verification2<Er>(
    ch: Sender<FprintMsg>,
    conn: &Con,
    dev: &Dev<'_>,
) -> Result<(), Er>
where
    Er: std::error::Error + Send + From<TransportError> + From<dbus::Error>,
{
    let devIface = "net.reactivated.Fprint.Device";
    let (_,): (Vec<String>,) = dev
        .method_call(devIface, "ListEnrolledFingers", ("",))
        .await
        .expect("Could not list the user fingerprints or the user has none");

    let (send_verifier, recv_verifier) = channel();

    let verifier = async move {
        let mut keep_verifying = true;
        while keep_verifying {
            let _: () = dev
                .method_call("net.reactivated.Fprint.Device", "VerifyStart", ("any",))
                .await
                .expect("Failed to start fingerprint verification");
            keep_verifying = recv_verifier.recv().unwrap_or(false);
            let _: () = dev
                .method_call("net.reactivated.Fprint.Device", "VerifyStop", ())
                .await
                .expect("Failed to stop fingerprint verification");
        }
    };

    let ch2 = ch.clone();
    let on_signal = move |_, (veredict, done): (String, _)| {
        let msg = match veredict.as_ref() {
            "verify-match" => FprintMsg::Sucess,
            "verify-no-match" => FprintMsg::NoMatch,
            "verify-retry-scan" => FprintMsg::NoMatch,
            "verify-finger-not-centered" => FprintMsg::NoMatch,
            "verify-remove-and-retry" => FprintMsg::NoMatch,
            "verify-swipe-too-short" => FprintMsg::NoMatch,
            _ => FprintMsg::Error(format!("Unknown state: {}", veredict)),
        };
        ch2.send(msg).expect("Failed to notify handler.");

        if done {
            let should_continue = veredict == "verify-no-match";
            send_verifier
                .send(should_continue)
                .expect("Failed to notify fingerprint verifier");
        }
        true
    };
    let mr = MatchRule::new_signal(devIface, "VerifyStatus");
    let verify_status_signal = conn.add_match(mr).await?.cb(on_signal);

    verifier.await;

    conn.remove_match(verify_status_signal.token()).await?;
    Ok(())
}

// Horrible hack... https://github.com/rust-lang/rust/issues/74497#issuecomment-849558488
fn force_static<T>(a: &T) -> &'static T {
    unsafe { &*(a as *const T) }
}

pub async fn continuous_verification<Er>(ch: Sender<FprintMsg>) -> Result<(), Er>
where
    Er: std::error::Error + Send + From<TransportError> + From<dbus::Error>,
{
    with_device(move |c, d| continuous_verification2(ch, c, d)).await
}

/// Hacky instance for testing, do not use...
impl From<dbus::Error> for TransportError {
    fn from(e: dbus::Error) -> Self {
        panic!("{:?}", e)
    }
}

#[tokio::main]
pub async fn main() -> Result<(), Box<dyn std::error::Error>> {
    let (send, recv) = channel();
    tokio::spawn(async {
        let _: Result<_, TransportError> = continuous_verification(send).await;
    });
    loop {
        println!("{:?}", recv.recv().unwrap());
    }
}
