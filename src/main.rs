use core::time;
use std::env;
use std::sync::mpsc::{channel, TryRecvError};
use std::thread;
use sysinfo::SystemExt;

use adw::prelude::{AdwApplicationWindowExt, BinExt};
use gtk::prelude::{BoxExt, ButtonExt, GtkWindowExt, OrientableExt, WidgetExt};
use relm4::{
    adw, gtk, send, AppUpdate, MessageHandler, Model, RelmApp, RelmMsgHandler, Sender, WidgetPlus,
    Widgets,
};

struct AppModel {
    parent_name: String,
    parent_msg: Option<String>,
    instructions: String,
    countdown: Option<u8>,
    closing: bool,
}

enum AppMsg {
    Cancel,
    CountDown,
    WillClose,
    CloseWindow,
}

#[derive(relm4::Components)]
struct AppComponents {
    timer: RelmMsgHandler<TimerComponent, AppModel>,
}

impl Model for AppModel {
    type Msg = AppMsg;
    type Widgets = AppWidgets;
    type Components = AppComponents;
}

impl AppUpdate for AppModel {
    fn update(
        &mut self,
        msg: AppMsg,
        components: &Self::Components,
        sender: Sender<AppMsg>,
    ) -> bool {
        match msg {
            AppMsg::CloseWindow => return false,
            AppMsg::Cancel => {
                self.countdown = None;
                self.instructions = "Authorization was denied".to_owned();
                components.timer.send(TimerMsg::StopTimer);
                send!(sender, AppMsg::WillClose)
            }
            AppMsg::CountDown => match self.countdown {
                Some(t) if t > 0 => {
                    self.countdown = Some(t - 1);
                }
                Some(0) => {
                    self.countdown = None;
                    self.instructions = "Authorization was refused due to a timeout".to_owned();
                    components.timer.send(TimerMsg::StopTimer);
                    send!(sender, AppMsg::WillClose)
                }
                _ => {}
            },
            AppMsg::WillClose => {
                self.closing = true;
                thread::spawn(move || {
                    thread::sleep(time::Duration::from_secs(1));
                    send!(sender, AppMsg::CloseWindow)
                });
            }
        };
        true // Keep the window alive...
    }
}

struct TimerComponent {
    sender: std::sync::mpsc::Sender<TimerMsg>,
}

enum TimerMsg {
    StopTimer,
}

impl MessageHandler<AppModel> for TimerComponent {
    type Msg = TimerMsg;
    type Sender = std::sync::mpsc::Sender<TimerMsg>;

    fn init(_parent_model: &AppModel, parent_sender: Sender<AppMsg>) -> Self {
        let should_stop = move |r: Result<Self::Msg, TryRecvError>| -> bool {
            match r {
                Err(TryRecvError::Disconnected) => true,
                Err(TryRecvError::Empty) => false,
                Ok(TimerMsg::StopTimer) => true,
            }
        };

        let (sender, receiver) = channel();
        thread::spawn(move || {
            while !should_stop(receiver.try_recv()) {
                thread::sleep(time::Duration::from_secs(1));
                send!(parent_sender, AppMsg::CountDown)
            }
        });
        TimerComponent { sender }
    }

    fn send(&self, msg: Self::Msg) {
        self.sender.send(msg).unwrap();
    }

    fn sender(&self) -> Self::Sender {
        self.sender.clone()
    }
}

#[relm4::widget]
impl Widgets<AppModel, ()> for AppWidgets {
    view! {
        adw::ApplicationWindow {
            set_default_width: 400,
            set_default_height: 200,
            set_resizable: false,
            set_content: main_box = Some(&gtk::Box) {
                set_orientation: gtk::Orientation::Vertical,
                append = &gtk::HeaderBar {
                    set_title_widget = Some(&gtk::Label) {
                        set_label: {
                            format!("Requesting fingerprint authentication for {}", model.parent_name).as_str()
                        },
                    }
                },
                append = &gtk::CenterBox {
                    set_orientation: gtk::Orientation::Vertical,
                    set_margin_all: 5,
                    set_vexpand: true,

                    set_start_widget = Some(&gtk::Label) {
                        set_margin_all: 5,
                        set_wrap: true,
                        set_label: match &model.parent_msg {
                            None => format!("{} is requesting consent through fingerprint authentication", model.parent_name),
                            Some(_) => format!("{} is requesting fingerprint authentication for the following action:", model.parent_name),
                        }.as_str(),
                    },
                    set_center_widget = Some(&adw::Bin) {
                        set_child = Some(&gtk::Label) {
                            set_margin_all: 15,
                            set_label: &model.parent_msg.clone().unwrap_or("".to_owned()),
                            set_wrap: true,
                        },
                        set_margin_all: 5,
                        set_vexpand: true,
                        set_css_classes: &["card"],
                        set_visible: model.parent_msg.is_some(),
                    },
                    set_end_widget = Some(&gtk::Box) {
                        set_orientation: gtk::Orientation::Vertical,
                        append = &gtk::Box {
                            set_orientation: gtk::Orientation::Horizontal,
                            set_halign: gtk::Align::End,
                            append = &gtk::Label {
                                set_wrap: true,
                                set_margin_all: 5,
                                set_label: watch! { &model.instructions },
                            },
                            append = &gtk::Button {
                                set_label: "Cancel",
                                set_sensitive: watch! { !model.closing },
                                connect_clicked(sender) => move |_| {
                                    send!(sender, AppMsg::Cancel);
                                },
                            },
                        },
                        append = &gtk::Label {
                            set_xalign: 1.0,
                            set_css_classes: &["caption"],
                            set_label: watch! {
                                &*match model.countdown {
                                    None => "".to_owned(),
                                    Some(t) => format!("Request will time out in {}s", t),
                                }
                            },
                        },
                    },
                }
            },
        }
    }
}

fn main() {
    let sys = sysinfo::System::new();
    let parent_name = match sys
        .get_process(sysinfo::get_current_pid())
        .and_then(|p| p.parent.and_then(|ppid| sys.get_process(ppid)))
    {
        None => "An unidentified application".to_owned(),
        Some(p) => format!("{}[{}]", p.name, p.pid),
    };
    let parent_msg = env::args().skip(1).last();

    let model = AppModel {
        parent_name,
        parent_msg,
        instructions: "Please swipe your fingerprint to authorize".to_owned(),
        countdown: Some(30),
        closing: false,
    };
    let app = RelmApp::new(model);
    app.run();
}
